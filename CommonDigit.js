function common_digit(a , b) {
    var map = new Map();
    var split = b.split(' ');
    var max = 0;

    if(split.length > a ) {
        max = a;
    } else {
        max = split.length;
    }

    for (let i = 0; i < max; i++) {
        let thenum = split[i].replace( /^\D+/g, '');
        for (let j = 0; j < thenum.length; j++) {
            let currentDigit = thenum[j];
            let key = Number(currentDigit);
            if (map.has(key)) {
                let value = map.get(key);
                map.set(key, value + 1 );
            } else {
                map.set(key, 1);
            }
        }
    }
    var maxKey = -1;
    var maxValue = -1;

    for (let [key, value] of map) {
        if(value > maxValue) {
            maxValue = value;
            maxKey = key;
        }
        if(value === maxValue) {
            maxValue = value;
            maxKey = key;
        }
    }
    console.log(maxKey);
    return maxKey;
}
common_digit(4, "14401 20 241 3 55 5 55 555");
common_digit(3, "111 222 5");
