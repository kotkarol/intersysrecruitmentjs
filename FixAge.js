
function fixage(a) {
    if(!Array.isArray(a)) {
        console.log("Provided argument is not an Array: " + a);
    } else {
        var resultArray = [];
        for (var i = 0; i < a.length; i++) {
            var currentNumber = +a[i];
            if(currentNumber >= 18 && currentNumber <= 60) {
                resultArray.push(currentNumber);
            }
        }
        if (resultArray.length === 0)
        {
            console.log("NA");
            return "NA";
        }
        var result = "";
        for (var i = 0; i < resultArray.length; i++)
        {
            if (i === resultArray.length - 1)
            {
                result = result + resultArray[i];
            } else {
                result = result + resultArray[i] + ".";
            }
        }
        console.log(resultArray);
        return resultArray;
    }
}
fixage([5,15,25,78,59,45]);
fixage([18,3,30,22,11,60]);
fixage([1,3,3,2,11,6]) ;