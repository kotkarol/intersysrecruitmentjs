function digit_sum(a) {
    if(!Array.isArray(a))
    {
        console.log("Provided argument is not an array.")
        return null;
    }
    let currentBiggestNumber = 0;
    let indexOfCurrentBiggest = 0;
    let currentBiggesValue = 0;
    for (let i = 0; i < a.length; i++) {
        var digits = (""+a[i]).split("");
        var singleSum = 0;
        for (let j = 0; j < digits.length; j++) {
            let currentDigit = Number(digits[j]);
            singleSum += currentDigit;
        }
        if (+singleSum > +currentBiggestNumber) {
            currentBiggestNumber = singleSum;
            indexOfCurrentBiggest = i;
            currentBiggesValue = a[i];
        }
        if(singleSum === currentBiggestNumber) {
            if (a[i] > currentBiggesValue) {
                currentBiggestNumber = singleSum;
                indexOfCurrentBiggest = i;
                currentBiggesValue = a[i];
            }
        }
    }
    console.log("Index of biggest: " + indexOfCurrentBiggest);
    return indexOfCurrentBiggest;
}
digit_sum([14401, 20, 241, 3, 55,888, 5, 55, 555]);
digit_sum([14401, 20, 241, 3, 55,888, 5, 55, 987]);
